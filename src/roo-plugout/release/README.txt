== RooPlugout
Copyright (C) 2010 - David Jeanneteau
Distributed under GPL v3

contact: plugout.roo@gmail.com


== INSTALL 
 -- debian/ubuntu (and other UNIX based systems)
 run script: sudo install_debian.sh
this script may also run on linux systems where bouml is installed in /usr/lib/bouml directory.

 -- windows
 run script install_win32.bat
 
	windows launcher is a .exe file, because the .bat launcher did not work

 -- other (ask me for assistance)
copy files and directories from 'release' to bouml install directory
update the plugout launcher file 



== TEST (kind of tutorial)
 - launch bouml
 - open project 'roo-project-test'
 - right click package 'test' (it has stereotype <<project>>)
 - select menu 'tool' > 'RooPlugout'
 - plugout is launched
 - the generated script is displayed in plugout console
 
The plugout does not modifies the UML model. 
 

== TODO
 - manage simple fields ( boolean, string, date )
 - manage complex fields ( relation in UML model)
 - output to files
 - manage more options (to be defined ..)
 
 
