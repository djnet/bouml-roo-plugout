BOUML_DIR=/usr/lib/bouml
RELEASE_DIR=.
NAME="$( cat project.name)"

# copy jar, launcher
cp $RELEASE_DIR/${NAME}.jar $BOUML_DIR
cp $RELEASE_DIR/${NAME} $BOUML_DIR

# copy project template
cp -r $RELEASE_DIR/roo-template $BOUML_DIR

# copy test project 
cp -r $RELEASE_DIR/roo-project-test $BOUML_DIR


