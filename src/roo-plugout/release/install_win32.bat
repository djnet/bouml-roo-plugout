set BOUML_DIR=C:\Program Files\Bouml
set RELEASE_DIR=.
set NAME=roo-plugout

REM copy jar, launcher
cp "%RELEASE_DIR%\%NAME%.jar" "%BOUML_DIR%"
cp "%RELEASE_DIR%\install-win32-build\%NAME%.exe" "%BOUML_DIR%\%NAME%.exe"

REM copy project template
cp -r "%RELEASE_DIR%\roo-template" "%BOUML_DIR%"

REM copy test project 
cp -r "%RELEASE_DIR%\roo-project-test" "%BOUML_DIR%"
